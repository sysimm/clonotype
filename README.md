# clonotype
cluster AIRR data using clonotyping. Prepare donor classification
features from clusters. 


## Usage

* make a list of AIRR files
Here we use all the Haindai 10x covid and healthy data (104
donors). These are ell ranger AIRR files, which are necessary because
we need the V and J gene names as well as CDRH3 AA sequence.

healthy heavy chain data is stored in "healthy_H_list" and COVID-19 data in "covid_H_list"

* search

`./src/clonotype_search.py -q ..E..D...S.... -n 14 -i healthy_H_list -v IGHV3-3 > healthy_hits.tsv` 
`./src/clonotype_search.py -q ..E..D...S.... -n 14 -i covid_H_list -v IGHV3-3 > covid_hits.tsv` 

where `-q` is a CDR3 motif (use '.' to allow any)
`-n` is the CDR3 length
`-v` is the v gene (use a smaller string to match more generally, e.g. "IGHV")

