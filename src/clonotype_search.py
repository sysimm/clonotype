#!/usr/bin/env python
import sys
import os
import tempfile
import shutil
import argparse
import pandas as pd
import re

def printf(format, *args):
    sys.stdout.write(format % args)


def fprintf(fp,format, *args):
    fp.write(format % args)


def applytab(row):
    print('\t'.join(map(str,row.values)))

def applytab_plus(x,row):
    print(x+'\t','\t'.join(map(str,row.values)))


def split_list_by_genes(list_in, run_dir, cdr3_motif, len_ok, vgene_ok, jgene_ok,interclone):



    cdr3name="junction_aa"
    if interclone:
        cdr3name="acdr3"

    columns = ["sequence_id", cdr3name]

    if vgene_ok:
        columns.append("v_call")

    if jgene_ok:
        columns.append("j_call")

    wrote_header = False

    for f in list_in:

        df = pd.read_table(f,sep="\t")

        if not wrote_header:

            applytab_plus("File",df.columns)
            wrote_header = True

        for c in columns:
            if c not in df.columns:
                raise Exception(c + " not found in input TSV file " + f)


        seqdat={}
        for index, row in df.iterrows():

            if type(row[cdr3name]) != str:
                continue

            vgene = ""
            if vgene_ok:
                vgene = str(row["v_call"])

            if jgene_ok:
                jgene = str(row["j_call"])

            cdr3aa = row[cdr3name][1:-1]            
            if interclone:
                cdr3aa = row[cdr3name]
            
            ilen = len(cdr3aa)
            slen = str(ilen)

            if len_ok and ilen != len_ok:
                continue



            if vgene_ok and (vgene not in vgene_ok and vgene_ok not in vgene):
                continue

            if jgene_ok and (jgene not in jgene_ok and jgene_ok not in jgene):
                continue


            if cdr3_motif:
                m = cdr3_motif.search(cdr3aa)
                if not m:
                    continue


            applytab_plus(f,row)

                    
                
def get_split_list(d):

    file_list=[]
    for root, dirs, files in os.walk(d):
        for f in files:
            if f == "cdr3.fa":
                file_list.append(os.path.join(root, f))

    return file_list

def read_list(list_file):

    list_out=[]
    with open(list_file,"r") as fh:
        for line in fh:
            f = line.strip()
            if os.path.isfile(f):
                list_out.append(f)
                
    return list_out

                
def main():

    parser = argparse.ArgumentParser(
        description='search by motif')
    parser.add_argument('-q', dest='query_motif', required=True,
                        help='query regular expression')
    parser.add_argument('-n', dest='cdr3_len', type=int,
                        help='allowed CDR3 len')
    parser.add_argument('-v', dest='vgene', default="IGH",
                        help='allowed vgenes')
    parser.add_argument('-j', dest='jgene', 
                        help='allowed jgenes')
    parser.add_argument('-i', dest='list_in', required=True,
                        help='tsv file list')
    parser.add_argument('-w', dest='work_dir', default="work",
                        help='working dir')
    parser.add_argument('--interclone', action='store_true',
                        help='interclone_processed')


    args = parser.parse_args()

    cdr3_motif = re.compile(args.query_motif)


    list_in = read_list(args.list_in)
    work_dir = args.work_dir
    vj_dir = os.path.join(work_dir)
    if not os.path.isdir(vj_dir):
        os.makedirs(vj_dir)

    split_list_by_genes(list_in,vj_dir,cdr3_motif,args.cdr3_len,args.vgene,args.jgene,args.interclone)




if __name__ == '__main__':
    main()



